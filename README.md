# StackStorm Comafi Pack
Paquete de instalación de Stackstorm del proyecto IaC para aprovisionamiento de infraestructura.

## Introducción
En el corriente repositorio se encuentran los fuentes necesarios para desplegar las acciones y sus manifiestos, los sensores, los workflows y las reglas de la plataforma Stackstorm del proyecto IaC para aprovisionamiento de infraestructura.

### Pre-Requisitos
_En tu entorno._
* [Stackstorm:]() Instancia con todos sus servicios desplegada y cliente CLI ```st2``` instalado.
* []()

## Instalación de StackStorm
#### Quick Install Bare Metal (Linux)

Grab a clean 64-bit Linux system that fits the system requirements. Make sure that curl is up to date using sudo apt-get install curl on Ubuntu, or sudo yum install curl nss on RHEL/CentOS. Then run this command:
```
curl -sSL https://stackstorm.com/packages/install.sh | bash -s -- --user=st2admin --password='Ch@ngeMe'
```
Documentación: https://docs.stackstorm.com/install/index.html

## Packs
In StackStorm, we call these integrations “Packs”. Packs are the atomic unit of deployment for integrations and extensions to StackStorm. This means that regardless of what you’re trying to implement, whether it’s a new Action, Sensor, Rule, or Sensor, it’s done with Packs.
Installing a new integration can be as simple as a one-line command. 

## Comafi Pack
#### Pre-requisitos
* Python 3 pip: ```sudo apt install pyhton-pip```
* Librerías de Kerberos para Ansible pack (Ubuntu 18.04): ``` sudo apt install libkrb5-dev```
* Librerías de Kerberos para Ansible pack (CentOS/RHEL 7):``` sudo yum install gcc krb5-devel```

```
st2 pack install --python3 https://gitlab.com/markoaular/stackstorm.git
st2 pack install --python3 ansible
```


### ST2 Client
```
st2 --version

# Get help. There's a lot to explore.
st2 -h

# Login - add "-w" to save the password if you like
# st2admin/Ch@ngeMe is the default username & password. Replace if needed
st2 login st2admin -p 'Ch@ngeMe'

# List the actions from the 'core' pack
st2 action list --pack=core
st2 trigger list
st2 rule list

# Run a local shell command
st2 run core.local -- date -R

# List of executions (most recent at the bottom)
st2 execution list

# Get execution by ID
st2 execution get <execution_id>

# Get only the last 5 executions
st2 execution list -n 5

# Make an API Key
st2 apikey create -k -m '{"used_by": "api test"}'

# Copy examples to st2 content directory and set permissions
sudo cp -r /usr/share/doc/st2/examples/ /opt/stackstorm/packs/
sudo chown -R root:st2packs /opt/stackstorm/packs/examples
sudo chmod -R g+w /opt/stackstorm/packs/examples

# Run setup
st2 run packs.setup_virtualenv packs=examples

# Reload stackstorm context
st2ctl reload --register-all

# Run a workflow poc using st2 client
st2 run comafi.poc-openshift-new-project body='{"kind":"Namespace","apiVersion":"v1","metadata":{"name":"project_name","creationTimestamp":null},"spec":{},"status":{}}' payload='{"short_description":"Create new project in Openshift"}'
```

### API REST
[StackStorm API Docs](https://api.stackstorm.com/) 
Comandos:
Para ver el manifiesto de la acción
```
curl -k -H "St2-Api-Key: <API Key>" https://<St2_server>/api/v1/actions/<nombre de paquete>.<nombre de acción> |jq 

curl -k -H "St2-Api-Key: <API Key>" https://<St2_server>/api/v1/apikeys

```
#### Para ejecutar un workflow desde la API
```
curl -k https://localhost/api/v1/webhooks/aws-deploy-ec2 -d '{"action": "run"}' -H 'Content-Type: applcation/json' -H 'St2-Api-Key: OTcwNzE5NDRkYjA0MDQ5Y2M5MTFiNjAwNzYxZWI4ZDFlYzY5OTlmZTdlMjcyY2FjNmM2MmIwZGRmMzA1NmVkNQ'

```

### Webhook
#### Ejecución de Webhook ```sample```
```
curl -k https://localhost/api/v1/webhooks/sample -d '{"foo": "ferluko desde webhook", "name": "st2"}' -H 'Content-Type: application/json' -H 'St2-Api-Key: MDZmYWNmYzRhYmZlMzRkNDVkNWM4YTBlMDFkNWQxNmJhYTg2NDkwZGJmZGUwNTI5ZGZkNGQzNWNlZGY2ZjEyMw'
```
#### Ejecución de Webhook ```aws```
```
curl -k https://localhost/api/v1/webhooks/aws -d '{"foo": "probando", "name": "st2"}' -H 'Content-Type: application/json' -H 'St2-Api-Key: OTcwNzE5NDRkYjA0MDQ5Y2M5MTFiNjAwNzYxZWI4ZDFlYzY5OTlmZTdlMjcyY2FjNmM2MmIwZGRmMzA1NmVkNQ'
```

### TIPs
#### Workflows
Es importante una task para abrir tickers previo a la ejecución del aprovisionamiento y dejar el ticket en "Open" state. Encontrar un tickets abiertos significa que la tarea de aprovisionamiento falló. Si el ticket está "resolved" el aprovisionamiento salió OK.
Los workflows se pueden editar directamente del servidor sin actualizar la configuración de st2.

### Actions 
Luego de la edición del manifiesto de los "actions" directamente del servidor correr: ```st2ctl reload --register-actions```. 

