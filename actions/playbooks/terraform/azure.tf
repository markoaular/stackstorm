
provider "azurerm" {
  
  version = "=1.44.0"

  subscription_id             = "IEx9x_Rl4pL2De0bb_4y2--7qW9TZI57.n"
  client_id                   = "e3b3961d-e188-47f7-bbb4-48c7562824e4"
  #client_certificate_path     = var.client_certificate_path
  #client_certificate_password = var.client_certificate_password
  tenant_id                   = "e3b3961d-e188-47f7-bbb4-48c7562824e4"
}



resource "azurerm_linux_virtual_machine" "st2" {
  name                = "linux-vm"
  resource_group_name = #########
  location            = #########
  size                = "Standard_F2"
  admin_username      = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.example.id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("~/.ssh/id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
}