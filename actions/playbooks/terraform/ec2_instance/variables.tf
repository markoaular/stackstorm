variable "cidr_vpc" {
  description = "CIDR block para vpc"
  default = "10.1.0.0/16"
}
variable "cidr_subnet" {
  description = "CIDR block para subnet"
  default = "10.1.0.0/24"
}
variable "availability_zone" {
  description = "availability zone subnet"
  default = "us-east-2a"
}
variable "public_key_path" {
  description = "Public key"
  default = "~/.ssh/id_rsa.pub"
}
variable "instance_ami" {
  description = "AMI para instancia"
  default = "ami-0cf31d971a3ca20d6"
}
variable "instance_type" {
  description = "tipo de instancia"
  default = "t2.micro"
}
variable "environment_tag" {
  description = "Tag environment"
  default = "Dev"
}