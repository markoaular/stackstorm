variable "secret_key" {
  type = string
}

variable "access_key" {
  type = string
}

#provider "aws" {
 #access_key = "${var.access_key}"
 #secret_key = "${var.secret_key}"
 #region = "us-east-2"
#}



#resource "aws_vpc" "vpc" {
 # cidr_block = "${var.cidr_vpc}"
  #enable_dns_support   = true
  #enable_dns_hostnames = true
  #tags = {
   #  Environment = "${var.environment_tag}"
  #}
#}



#resource "aws_internet_gateway" "igw" {
 # vpc_id = "${aws_vpc.vpc.id}"
 # tags = {
  #   Environment = "${var.environment_tag}"
  #}
#}

#resource "aws_subnet" "subnet_public" {
 # vpc_id = "${aws_vpc.vpc.id}"
  #cidr_block = "${var.cidr_subnet}"
  #map_public_ip_on_launch = "true"
  #availability_zone = "${var.availability_zone}"
  #tags = {
   #  Environment = "${var.environment_tag}"
  #}
#}

#resource "aws_route_table" "rtb_public" {
 # vpc_id = "${aws_vpc.vpc.id}"
#route {
 #     cidr_block = "0.0.0.0/0"
  #    gateway_id = "${aws_internet_gateway.igw.id}"
  #}
#tags = {
 #   Environment = "${var.environment_tag}"
  #}
#}


#resource "aws_route_table_association" "rta_subnet_public" {
 # subnet_id      = "${aws_subnet.subnet_public.id}"
  #route_table_id = "${aws_route_table.rtb_public.id}"
#}


#resource "aws_security_group" "sg_22" {
 # name = "sg_22"
  #vpc_id = "${aws_vpc.vpc.id}"
  #ingress {
   #   from_port   = 22
    #  to_port     = 22
     # protocol    = "tcp"
      #cidr_blocks = ["0.0.0.0/0"]
  #}
 #egress {
  #  from_port   = 0
   # to_port     = 0
    #protocol    = "-1"
    #cidr_blocks = ["0.0.0.0/0"]
  #}
  #tags = {
   # Environment = "${var.environment_tag}"
  #}
#}


#resource "aws_instance" "demo-stackstorm" {
 # ami           = "${var.instance_ami}"
  #instance_type = "${var.instance_type}"
  #subnet_id = "${aws_subnet.subnet_public.id}"
  #associate_public_ip_address = true
  #vpc_security_group_ids = ["${aws_security_group.sg_22.id}"]
  #key_name = "st2-demo"
#tags = {
 #  Environment = "${var.environment_tag}"
  # name        = "demo-stackstorm"
 #}
#}

##################################### 

provider "aws" {
  region = "us-east-1"
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
}

resource "aws_instance" "instance-deploy" {
  ami           = "ami-0947d2ba12ee1ff75"
  instance_type = "t2.micro"
  subnet_id = "subnet-006145a3ead20db8e"
  #associate_public_ip_address = true
  vpc_security_group_ids = ["sg-0a1f57c25190e7daf"]
  key_name = "CMF-SEMPERTI-EC2-POC"
tags = {
   name        = "ec2-instance"
 }
}


#resource "aws_eip" "ip" {
#  vpc      = true
#  instance = aws_instance.demo-stackstorm.id
#}

