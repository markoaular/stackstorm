output "aws_ip" {
  value = aws_instance.instance-deploy.public_ip
}
output "public_dns"{
  value = aws_instance.instance-deploy.public_dns
}