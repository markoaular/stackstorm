[![StackStorm](https://github.com/stackstorm/st2/raw/master/stackstorm_logo.png)](https://www.stackstorm.com)

**StackStorm** is a platform for integration and automation across services and tools, taking actions in response to events. Learn more at [www.stackstorm.com](http://www.stackstorm.com/product).

[![Travis Unit Tests Status](https://travis-ci.org/StackStorm/st2.svg?branch=master)](https://travis-ci.org/StackStorm/st2) 
[![Packages Build Status](https://circleci.com/gh/StackStorm/st2/tree/master.svg?style=shield)](https://circleci.com/gh/StackStorm/st2) 
[![Codecov](https://codecov.io/github/StackStorm/st2/badge.svg?branch=master&service=github)](https://codecov.io/github/StackStorm/st2?branch=master) 
[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1833/badge)](https://bestpractices.coreinfrastructure.org/projects/1833) 
![Python 2.7 | 3.6](https://img.shields.io/badge/python-2.7%20%7C%203.6-blue) 
[![Apache Licensed](https://img.shields.io/github/license/StackStorm/st2)](LICENSE) 
[![Join our community Slack](https://img.shields.io/badge/slack-stackstorm-success.svg?logo=slack)](https://stackstorm.com/community-signup) 
[![Forum](https://img.shields.io/discourse/https/forum.stackstorm.com/posts.svg)](https://forum.stackstorm.com/)

---
## Content 

1. [Audience](##Audience)
1. [Objetives](##Objetives)
1. [Introduction to StackStorm](##Introduction-to-StackStorm)
    * [About](###About)
    * [Main uses cases](###Main-uses-cases)
    * [How it works](###How-it-works)
1. [Installation](##Installation)
1. [Architecture](##Architecture)
    * [Event Driven Architecture](##Event-Driven-Architecture)
    * [High Availability Deployment](##High-Availability-Deployment)
1. [Workflow](##Workflow)
    * [Orquesta Workflow Engine](##Orquesta-Workflow-Engine)
    * [StackStorm Workflow Editor - st2flow](##WorStackStorm-Workflow-Editor-st2flows)
1. [Packs](##Packs)
    * [What is a Pack?](##What-is-a-Pack?)
    * [Anatomy of a Pack](##Anatomy-of-a-Pack)
    * [StackStorm Exchange](##StackStorm-Exchange)
    * [StackStorm Exchange Pack Template](##StackStorm-Exchange-Pack-Template)
1. [Official Documentation](##Official-Documentation)
1. [Security](##Security)
1. [Copyright, License, and Contributor Agreement](##Copyright)
---
## Audience
Since the following documentation is an introduction to this platform reviewing concepts in a high level perspective, the proposed audience is generalist from a help desk user to an expert IT architect.

## Objetives
As was defined before, the propose of this documentation is an introduction to concepts in order to gain a better understanding of the platform. It's recommended the [official documentation](##Official) for beyond information.

## Introduction to StackStorm
### About

StackStorm is a platform for integration and automation across services and tools. It ties together your existing infrastructure and application environment so you can more easily automate that environment -- with a particular focus on taking actions in response to events.

StackStorm helps automate common operational patterns.
![StackStorm use cases](./images/use-cases.png)

### Main uses cases
* **Assisted/Facilitated Troubleshooting** - triggering on system failures captured by Zabbix, Nagios, Sensu, New Relic and other monitoring, running a series of diagnostic checks on physical nodes, VMware or Amazon instances, and application components, and posting results to a shared communication context, like ServiceNow or JIRA.
* **Automated remediation** - identifying and verifying hardware failure on OpenStack compute node, properly evacuating instances and emailing VM about potential downtime, but if anything goes wrong - freezing the workflow and calling PagerDuty to wake up a human.
* **Continuous deployment** - build and test with Jenkins, provision a new AWS cluster, turn on some traffic with the load balancer, and roll-forth or roll-back based on NewRelic app performance data.

StackStorm helps you compose these and other operational patterns as rules and workflows or actions; and these rules and workflows - the content within the StackStorm platform - are stored *as code* which means they support the same approach to collaboration that you use today for code development and can be shared with the broader open source community via [StackStorm Exchange](##StackStormExchange).


### How it works

#### StackStorm High Level Architecture

![StackStorm architecture diagram](./images/92291633-6b5aae00-eece-11ea-912e-3bf977aa3cea.png)

StackStorm plugs into the environment via an extensible set of adapters: sensors and actions.

* **Sensors** are Python plugins for inbound integration that watch for events from external systems and fire a StackStorm trigger when an event happens.

* **Triggers** are StackStorm representations of external events. There are generic triggers (e.g., timers, webhooks) and integration triggers (e.g., Sensu alert, JIRA issue updated). A new trigger type can be defined by writing a sensor plugin.

* **Actions** are StackStorm outbound integrations. There are generic actions (SSH, HTTP request), integrations (OpenStack, Docker, Puppet), or custom actions. Actions are either Python plugins, or any scripts, consumed into StackStorm by adding a few lines of metadata. Actions can be invoked directly by user via CLI, API, or the web UI, or used and called as part of automations - rules and workflows.

* **Rules** map triggers to actions (or to workflows), applying matching criterias and map trigger payload data to action inputs.

* **Workflows** stitch actions together into "uber-actions", defining the order, transition conditions, and passing context data from one action to the next. Most automations are multi-step (eg: more than one action). Workflows, just like "atomic" actions, are available in the action library, and can be invoked manually or triggered by rules.

* **Packs** are the units of content deployment. They simplify the management and sharing of StackStorm pluggable content by grouping integrations (triggers and actions) and automations (rules and workflows). A growing number of packs is available on the StackStorm Exchange. Users can create their own packs,  share them on GitHub, or submit them to the StackStorm Exchange organization.

* **Audit trail** is the historical list of action executions, manual or automated, and is recorded and stored with full details of triggering context and execution results. It is is also captured in audit logs for integrating with external logging and analytical tools: LogStash, Splunk, statsd, or syslog.

## Architecture

### Event Driven Architecture
![StackStorm architecture diagram](./images/event-driven-arch.png)

StackStorm is a service with modular architecture. It is comprised of loosely coupled microservice components that communicate over a message bus, and scales horizontally to deliver automation at scale. StackStorm has a full REST API, CLI client, and web UI for admins and users to operate it locally or remotely, as well as Python client bindings for developer convenience.

### High Availability Deployment

Trought K8s Helm Chart that codifies StackStorm (aka "IFTTT for Ops" https://stackstorm.com/) Highly Availability fleet as a simple to use reproducible infrastructure-as-code app.

It will install 2 replicas for each component of StackStorm microservices for redundancy, as well as backends like RabbitMQ HA, MongoDB HA Replicaset and etcd cluster that st2 replies on for MQ, DB and distributed coordination respectively.

![StackStorm HA Deployment](./images/ha-deploy.png)

## Installation
StackStorm is distributed as RPMs and Debs for RedHat/CentOS and Ubuntu Linux systems, and as Docker images. You can either use a script to automatically install and configure all components on a single system, or you can follow the manual instructions for your OS.

Here’s an overview of the options:

One-line Install: Run our installation script for an opinionated install of all components on a single system. This is a our recommended way to get started. See the Quick Install section below for details.

* **Manual Installation:** Have custom needs? Maybe no Internet access from your servers? Or just don’t like using scripted installs? Read the manual installation instructions for your OS (Ubuntu 16, Ubuntu 18, RHEL/CentOS 6, RHEL/CentOS 7, RHEL/CentOS 8) and adapt them to your needs. Here’s some additional guidance for setting up an internal mirror for the ST2 repos.

* **Vagrant / Virtual Appliance:** Vagrant / OVA is a quick and easy way to try StackStorm. It’s already pre-installed, tested and shipped as a virtual image and so saves your time going through time-consuming installation and configuration steps. Works best as a testing, pack development or demo system and recommended to get familiar with StackStorm platform. vagrant init stackstorm/st2 && vagrant up is all you need to get started. See Vagrant for more detailed instructions.

* **Docker:** StackStorm is supported on Docker - check out our Docker instructions. It’s one of the quickest way to get StackStorm running and useful for trying the platform and development.

* **Ansible Playbooks:** If you are an Ansible user, check these Ansible Playbooks for installing StackStorm. Ideal for repeatable, consistent, idempotent installation of StackStorm.

* **Puppet Module:** For Puppet users, check this Puppet Module for installing StackStorm. A robust and idempotent method of installing and configuring StackStorm.

* **High Availability** Entrusting business critical automation tasks to a system like StackStorm leads to higher demands on that system. StackStorm can run in a HA mode to ensure these needs. St2 HA Cluster in Kubernetes - BETA automates entire complex infrastructure as a reproducible blueprint.

## Workflows
Typical datacenter operations and processes involve taking multiple actions across various systems. To capture and automate these operations, StackStorm uses workflows. A workflow strings atomic actions into a higher level automation, and orchestrates their executions by calling the right action, at the right time, with the right input. It keeps state, passes data between actions, and provides reliability and transparency to the execution.

Just like any actions, workflows are exposed in the automation library, and can be called manually, or triggered by rules. Workflows can even be called from other workflows.

To create a workflow action, choose a workflow runner, connect the actions in a workflow definition, and provide the usual action meta data.

### Orquesta Workflow Engine

Orquesta is a graph based workflow engine designed specifically for
[StackStorm](https://github.com/StackStorm/st2). As a building block, Orquesta does not include all the parts such as messaging, persistence, and locking required to run as a service.

The engine consists of the workflow models that are decomposed from the language spec, the composer that composes the execution graph from the workflow models, and the conductor that directs the execution of the workflow using the graph.

A workflow definition is a structured YAML file that describes the intent of the workflow. A workflow is made up of one or more tasks. A task defines what action to execute, with what input.
When a task completes, it can transition into other tasks based upon criteria. Tasks can also publish output for the next tasks. When there are no more tasks to execute, the workflow is complete.

Orquesta includes a native language spec for the workflow definition. The language spec is decomposed into various models and described with [JSON schema](http://json-schema.org/). A workflow composer that understands the models converts the workflow definition into a directed graph. The nodes represent the tasks and edges are the task transition. The criteria for task transition is an attribute of the edge. The graph is the underpinning for conducting the workflow execution. The workflow definition is just syntactic sugar.

Orquesta allows for one or more language specs to be defined. So as long as the workflow definition, however structured, is composed into the expected graph, the workflow conductor can handle it.

The workflow execution graph can be a directed graph or a directed cycle graph. It can have one or more root nodes which are the starting tasks for the workflow. The graph can have branches that run in parallel and then converge back to a single branch. A single branch in the graph can diverge into multiple branches. The graph model exposes operations to identify starting tasks, get inbound and outbound task transitions, get connected tasks, and check if cycle exists. The graph serves more 
like a map for the conductor. It is stateless and does not contain any runtime data such as task status and result.

The workflow conductor traverses the graph, directs the flow of the workflow execution, and tracks runtime state of the execution. The conductor does not actually execute the action that is specified for the task. The action execution is perform by another provider such as StackStorm. The conductor directs the provider on what action to execute. As each action execution completes, the provider relays the status and result back to the conductor. The conductor then takes the state change, keeps track of the sequence of task execution, manages change history of the runtime context, evaluate outbound task transitions, identifies any new tasks for execution, and determines the overall workflow state and result.

### StackStorm Workflow Editor - st2flow
Visual editor (web-based GUI) for creating and updating StackStorm workflows. This editor is not longer an enterprise feature, now it's totally free to use.

![StackStorm workflof Designer](./images/workflow-designer1.gif)

All your workflows are still stored as code on your system, but the GUI simplifies the workflow development process. Updates to the graphical view are immediately updated in the underlying code, and vice-versa.


#### Requirements
- Node v10
- Lerna, Yarn, and Gulp

    ```
    sudo npm install -g gulp-cli lerna yarn
    ```

#### Installation

This project uses Lerna and should be considered a "subset" of the [st2web](https://github.com/StackStorm/st2web) project. It is currently required that the st2web repo be cloned next to the st2flow repo:

```
├── StackStorm
|   ├── st2flow
|   └── st2web
```

Here is the basic flow of commands to get started:

```
git clone git@github.com:StackStorm/st2web.git
# ...or https: https://github.com/StackStorm/st2web.git

cd st2web
lerna bootstrap

cd ../
git clone git@github.com:StackStorm/st2flow.git
# ...or https: https://github.com/StackStorm/st2flow.git

cd st2flow
lerna bootstrap
```
#### Running the app and tests
```
# Run the app, available on http://localhost:3000
# Note, it takes ~10 - 20s to get running but then you're in watch mode
gulp

# Run unit tests
gulp test-unit

# Linting
gulp lint
```

## Packs
### What is a Pack?
A “pack” is the unit of deployment for integrations and automations that extend StackStorm. Typically a pack is organized along service or product boundaries e.g. AWS, Ansible, ServiceNow etc. A pack can contain Actions, Workflows, Rules, Sensors, and Aliases. StackStorm content is always part of a pack, so it’s important to understand how to create packs and work with them.

Some packs extend StackStorm to integrate it with external systems — like AWS, GitHub, or JIRA. We call them “integration packs”. Some packs capture automation patterns — they contain workflows, rules, and actions for a specific automation process — like the st2 demo pack. We call them “automation packs”. This naming is mostly a convention: StackStorm itself makes no distinction between the two.

Integration packs can be shared and reused by anyone who uses the service that pack is built for. You can find many examples of these at the StackStorm Exchange. Automation packs are often very site-specific and have little use outside of a particular team or company; they are usually shared internally.

### Anatomy of a Pack
The canonical pack filesystem layout looks like this:
```
# contents of a pack folder
actions/                 #
rules/                   #
sensors/                 #
aliases/                 #
policies/                #
tests/                   #
etc/                     # any additional things (e.g code generators, scripts...)
config.schema.yaml       # configuration schema
packname.yaml.example    # example of config, used in CI
pack.yaml                # pack definition file
requirements.txt         # requirements for Python packs
requirements-tests.txt   # requirements for python tests
icon.png                 # 64x64 .png icon
```
### StackStorm Exchange
The marketplace for the integrations packs. Thare are more than 170 integrations already done regarding to:
* Monitoring
* Networking
* SaaS, IaC, IaaS, PaaS
* Cloud providers
* Ci/CD
* SO and their core functions
* IoT
* More in https://exchange.stackstorm.org/

### StackStorm Exchange Pack Template
This is a basic template directory structure for a StackStorm Integration Pack.

#### How to Use

1. Clone this repository, and rename the directory to your pack name.
2. Edit `pack.yaml`. Make sure you set the pack name, the description, and add some meaningful
   keywords.
3. Add any Python dependencies to `requirements.txt`
4. Edit `config.schema.yaml`. This contains the schema for pack-specific configuration items.
5. Move `template.yaml.example` to `<pack_name>.yaml.example`, and add some example
   configuration. This will get validated against your configuration schema.
6. Add your actions, sensors and workflows. Need code ideas? Check [Github](https://github.com/StackStorm-Exchange/)!
8. Test your pack, and when it's ready, submit to [exchange-incubator](https://github.com/StackStorm-Exchange/exchange-incubator)


## Official Documentation

Additional documentation, including installation proceduces, action/rule/workflow authoring, and how to setup and use triggers/sensors can be found at [https://docs.stackstorm.com](https://docs.stackstorm.com).

Any problems or questions? hit StackStorm up on [Slack](https://stackstorm.com/community-signup)

## Security
### How Are Vulnerabilities Handled
StackStorm follows the industry de facto standard of Responsible Disclosure for handling security issues. This means StackStorm discloses the issue only after a fix or mitigation for the issue has been released.

StackStorm of course always gives full credit to people who have reported the issue.

For more information, please refer to https://docs.stackstorm.com/latest/security.html

## Copyright, License, and Contributor Agreement

Copyright 2020 The StackStorm Authors.
Copyright 2019 Extreme Networks, Inc.
Copyright 2014-2018 StackStorm, Inc.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this work except in compliance with the License. You may obtain a copy of the License in the [LICENSE](LICENSE) file, or at:

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

By contributing you agree that these contributions are your own (or approved by your employer) and you grant a full, complete, irrevocable copyright license to all users and developers of the project, present and future, pursuant to the license of the project.